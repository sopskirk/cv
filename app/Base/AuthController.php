<?php

namespace App\Base;

use Illuminate\Support\Facades\Auth;

/**
 * Class Controller
 * @package App\Base
 */
abstract class AuthController extends Controller
{
    /** @var Auth */
    protected $auth;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->auth = $auth = Auth::guard('user');

        $this->middleware(
            static function ($request, $next) use ($auth) {
                view()->share('user', $auth->user());

                return $next($request);
            }
        );
    }
}
