<?php


namespace App\Base;

use App\Interfaces\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Repository
 * @package App\Base
 */
abstract class Repository implements RepositoryInterface
{
    /**
     * model property on class instances
     * @var Model|Builder
     */
    protected $model;

    /**
     * Constructor to bind model to repo
     * Repository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get all instances of model
     * @return Collection|Model[]
     */
    public function all()
    {
        return $this->model::all();
    }

    /**
     * create a new record in the database
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * update record in the database
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id)
    {
        $record = $this->model->find($id);
        return $record->update($data);
    }

    /**
     * remove record from the database
     * @param $id
     * @return int
     */
    public function delete($id): int
    {
        return $this->model::destroy($id);
    }

    /**
     * show the record with the given id
     * @param $id
     * @return Builder|Model
     */
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Get the associated model
     * @return Builder|Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set the associated model
     * @param $model
     * @return object
     */
    public function setModel($model): object
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Eager load database relationships
     * @param $relations
     * @return Builder
     */
    public function with($relations): Builder
    {
        return $this->model->with($relations);
    }
}
