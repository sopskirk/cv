<?php

namespace App\Base;

use Illuminate\Http\Response as HttpResponse;

/**
 * Class Response
 * @package App\Base
 */
class Response
{
    /**
     * @param array $data
     * @param int $status
     * @return HttpResponse
     */
    public static function json(array $data, int $status = 200): HttpResponse
    {
        $content = [
            'status' => $status < 400,
            'data' => $data,
        ];

        return (new HttpResponse($content, $status))
            ->header('Content-Type', 'application/json');
    }

    /**
     * @param array $data
     * @param int $status
     * @return HttpResponse
     */
    public static function errorJson(array $data, int $status = 422): HttpResponse
    {
        return self::json($data, $status);
    }
}
