<?php

namespace App\Guards;

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserGuardService
 * @package App\Guards
 */
class UserGuard implements Guard
{
    /** @var User */
    private $user;

    /**
     * UserGuardService constructor.
     */
    public function __construct()
    {
        if (Auth::guard('user')->check()) {
            try {
                /** @var User $user */
                $this->user = Auth::guard('user')->user()->user;
            } catch (\Exception $exception) {
            }
        }
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return $this->user() instanceof User;
    }

    /**
     * @return bool
     */
    public function guest(): bool
    {
        return !$this->check();
    }

    /**
     * @return User|Authenticatable|null
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * @return int|string|null
     */
    public function id()
    {
        if ($this->check()) {
            return $this->user()->id;
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function validate(array $credentials = [])
    {
        // TODO: Implement validate() method.
    }

    /**
     * @inheritDoc
     */
    public function setUser(Authenticatable $user)
    {
        // TODO: Implement setUser() method.
    }
}
