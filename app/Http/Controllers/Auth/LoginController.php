<?php


namespace App\Http\Controllers\Auth;

use App\Base\Controller;
use App\Base\Response;
use App\Http\Requests\Auth\LoginRequest;
use App\Interfaces\Repositories\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('auth.login');
    }

    /**
     * @param LoginRequest $request
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function ajax(
        LoginRequest $request,
        UserRepositoryInterface $userRepository
    ): \Illuminate\Http\Response {
        $validated = $request->validated();

        try {
            /** @var User $user */
            $user = $userRepository->findUserByEmail($validated['email']);

            // User not exists
            if ($user === null) {
                return Response::json(
                    [
                        'error' => 'Podany adres e-mail nie istnieje!',
                        'status' => false,
                    ]
                );
            }

            // User account is not active
            if (!$user->active) {
                return Response::json(
                    [
                        'error' => 'Konto nie jest aktywne!',
                        'status' => false,
                    ]
                );
            }

            // Check password
            if (!Hash::check($validated['password'], $user->password)) {
                return Response::json(
                    [
                        'error' => 'Hasło jest nieprawidłowe!',
                        'status' => false,
                    ]
                );
            }

            // Account verified
            if (!$user->verified) {
                return Response::json(
                    [
                        'error' => 'Musisz potwierdzić swoje konto. Wysłaliśmy Ci kod aktywacyjny, sprawdź swój adres e-mail.',
                        'status' => false,
                    ]
                );
            }

            // Login user
            Auth::guard('user')->login($user);
            $user->update(['last_visit' => date('Y-m-d H:i:s')]);

            return Response::json(['status' => true]);
        } catch (\Exception $e) {
            return Response::json(['status' => false], 500);
        }
    }
}
