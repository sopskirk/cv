<?php


namespace App\Http\Controllers\Auth;


use App\Base\Controller;
use App\Base\Response;
use App\Http\Requests\Auth\RegisterRequest;
use App\Interfaces\MailgunInterface;
use App\Interfaces\Repositories\RepositoryInterface;
use App\Interfaces\Repositories\UserRepositoryInterface;
use App\Models\User;
use App\Models\VerifyUser;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use PhpParser\Builder;
use Throwable;
use Faker\Factory;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /** @var MailgunInterface */
    private $mailgun;

    /** @var bool */
    private $mailgunStatus = false;

    /**
     * RegisterController constructor.
     * @param MailgunInterface $mailgun
     */
    public function __construct(MailgunInterface $mailgun)
    {
        $this->mailgun = $mailgun;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return view('auth.register');
    }

    /**
     * @param RegisterRequest $request
     * @param UserRepositoryInterface|RepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function ajax(RegisterRequest $request, UserRepositoryInterface $userRepository): \Illuminate\Http\Response {
        $validated = $request->validated();

        // User exists?
        if ($userRepository->findUserByEmail($validated['email']) !== null) {
            return Response::json(
                [
                    'status' => false,
                    'error' => 'Podany adres e-mail już istnieje!',
                ]
            );
        }

        DB::beginTransaction();

        try {
            // Create a new user
            $user = $userRepository->create(
                [
                    'email' => $validated['email'],
                    'password' => Hash::make($validated['password']),
                    'name' => $validated['name'],
                ]
            );

            // Send verification link
            $this->sendVerifyLink($user);

            // Error send activate link
            if (!$this->mailgunStatus) {
                DB::rollBack();

                return Response::json(
                    [
                        'status' => false,
                        'error' => 'Wiadomość e-mail nie została wysłana. Odswież stronę i spróbuj ponownie!',
                    ]
                );
            }

            DB::commit();

            return Response::json(['status' => true]);
        } catch (Throwable $e) {
            DB::rollBack();

            return Response::json(['status' => false], 500);
        }
    }

    /**
     * @param string $token
     * @return RedirectResponse
     */
    public function verify(string $token): RedirectResponse
    {
        $verifyUser = VerifyUser::query()->where('token', $token)->first();
        $key = 'status';

        if (isset($verifyUser)) {
            $user = $verifyUser->user;

            if (!$user->verified) {
                $verifyUser->user->verified = true;
                $verifyUser->user->saveOrFail();

                $status = 'Twój e-mail został zweryfikowany. Teraz możesz się zalogować.';
            } else {
                $status = 'Twój e-mail został już zweryfikowany.';
                $key = 'warning';
            }
        } else {
            return redirect()
                ->route('auth.login.index')
                ->with('warning', 'Przepraszamy, nie można zidentyfikować Twojego adresu e-mail.');
        }

        return redirect()
            ->route('auth.login.index')
            ->with($key, $status);
    }

    /**
     * @param User $user
     * @return void
     * @throws Throwable
     */
    private function sendVerifyLink(User $user): void
    {
        $faker = Factory::create();

        /** @var VerifyUser|Builder $verifyUser */
        $verifyUser = VerifyUser::query()->make(['token' => $faker->unique()->regexify('[A-Za-z0-9]{40}')]);

        $verifyUser->user()->associate($user);
        $verifyUser->saveOrFail();

        $this->mailgunStatus = $this->mailgun->send(
            [
                'to' => $user->email,
                'subject' => 'Weryfikacja adresu e-mail',
                'html' => [
                    'view' => 'verifyUser',
                    'params' => compact('user'),
                ],
            ]
        );

        back()->with(
            'status',
            'Wysłaliśmy Ci kod aktywacyjny. Sprawdź swój adres e-mail i kliknij link, aby zweryfikować.'
        );
    }
}
