<?php


namespace App\Http\Controllers\Logged;

use App\Base\AuthController;
use Illuminate\View\View;

/**
 * Class HomeController
 * @package App\Http\Controllers\Logged
 */
class HomeController extends AuthController
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('pages.logged.index');
    }
}
