<?php


namespace App\Interfaces;

use App\Services\MailgunService;
use Mailgun\Model\Domain\DnsRecord;

/**
 * Interface MailgunService
 * @package App\Interfaces
 */
interface MailgunInterface
{
    /**
     * @return MailgunService
     */
    public function debug(): MailgunService;

    /**
     * @param array $params
     * @return bool
     */
    public function send(array $params): bool;

    /**
     * @return array|DnsRecord[]
     */
    public function response(): array;
}
