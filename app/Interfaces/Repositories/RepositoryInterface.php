<?php


namespace App\Interfaces\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface RepositoryInterface
 * @package App\Interfaces\Repositories
 */
interface RepositoryInterface
{
    /**
     * @return Collection|Model[]
     */
    public function all();

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id);

    /**
     * @param $id
     * @return int
     */
    public function delete($id): int;

    /**
     * @param $id
     * @return Builder|Model
     */
    public function show($id);

    /**
     * @return Builder|Model
     */
    public function getModel();

    /**
     * @param $model
     * @return object
     */
    public function setModel($model): object;

    /**
     * @param $relations
     * @return Builder
     */
    public function with($relations): Builder;
}
