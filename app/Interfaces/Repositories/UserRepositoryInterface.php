<?php


namespace App\Interfaces\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface UserRepositoryInterface
 * @package App\Interfaces\Repositories
 */
interface UserRepositoryInterface
{
    /**
     * @param string $email
     * @return Builder|Model
     */
    public function findUserByEmail(string $email);
}
