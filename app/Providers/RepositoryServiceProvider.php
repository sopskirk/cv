<?php

namespace App\Providers;

use App\Base\Repository;
use App\Interfaces\Repositories\CourseLessonRepositoryInterface;
use App\Interfaces\Repositories\CourseRepositoryInterface;
use App\Interfaces\Repositories\OrderRepositoryInterface;
use App\Interfaces\Repositories\ShopProductRepositoryInterface;
use App\Interfaces\Repositories\ShopRepositoryInterface;
use App\Interfaces\Repositories\UserCourseRepositoryInterface;
use App\Interfaces\Repositories\UserLessonRepositoryInterface;
use App\Interfaces\Repositories\UserQuizAnswerRepositoryInterface;
use App\Interfaces\Repositories\UserQuizRepositoryInterface;
use App\Interfaces\Repositories\UserRepositoryInterface;
use App\Interfaces\Repositories\RepositoryInterface;
use App\Repositories\CourseLessonRepository;
use App\Repositories\CourseRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ShopProductRepository;
use App\Repositories\ShopRepository;
use App\Repositories\UserCourseRepository;
use App\Repositories\UserLessonRepository;
use App\Repositories\UserQuizAnswerRepository;
use App\Repositories\UserQuizRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RepositoryInterface::class, Repository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
