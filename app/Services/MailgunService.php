<?php


namespace App\Services;

use App\Interfaces\MailgunInterface;
use Mailgun\HttpClient\HttpClientConfigurator;
use Mailgun\Mailgun;
use Mailgun\Model\Domain\DnsRecord;
use Throwable;

/**
 * Class MailgunService
 * @package App\Services
 */
class MailgunService implements MailgunInterface
{
    /** @var Mailgun */
    private $mailgun;

    /**
     * MailgunService constructor.
     */
    public function __construct()
    {
        $this->mailgun = Mailgun::create(env('MAILGUN_SECRET'));
    }

    /**
     * @return MailgunService
     */
    public function debug(): MailgunService
    {
        $configurator = new HttpClientConfigurator();
        $configurator->setEndpoint(env('MAILGUN_ENDPOINT'));
        $configurator->setDebug(true);

        $this->mailgun = new Mailgun($configurator);

        return $this;
    }

    /**
     * @param array $params
     * @return bool
     * @throws Throwable
     */
    public function send(array $params): bool
    {
        if (!array_key_exists('from', $params)) {
            $params['from'] = env('MAIL_FROM_ADDRESS');
        }

        if (array_key_exists('html', $params)) {
            $html = $params['html'];
            $params['html'] = view('emails.'.$html['view'], $html['params'])->render();
        }

        $response = $this->mailgun->messages()->send(env('MAILGUN_DOMAIN'), $params);
        return $response->getMessage() === 'Queued. Thank you.';
    }

    /**
     * @return array|DnsRecord[]
     */
    public function response(): array
    {
        return $this->mailgun->domains()->show(env('MAILGUN_DOMAIN'))->getInboundDNSRecords();
    }
}
