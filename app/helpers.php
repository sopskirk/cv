<?php

declare(strict_types=1);

use Illuminate\Support\Facades\File;

if (!function_exists('version')) {
    /**
     * @param $fileName
     *
     * @return string
     */
    function version($fileName)
    {
        $path = public_path('assets'.'/'.$fileName);

        return asset($fileName).'?='.(File::lastModified($path));
    }
}
