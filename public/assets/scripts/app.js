// App
const App = function () {
    /**
     *
     * @param issetlet
     * @param caselet
     * @returns {*}
     */
    const handleIsset = function (issetlet, caselet) {
        return issetlet !== undefined && issetlet !== null ? issetlet : caselet;
    };

    /**
     *
     * @param text
     * @param title
     * @param type
     * @returns {*|void}
     */
    const handleAlert = function (text, title, type) {
        return swal.fire({
            type: handleIsset(type, 'error'),
            title: handleIsset(title, 'Wystapił błąd!'),
            html: handleIsset(text, 'Wszystkie pola są wymagane!')
        });
    };

    /**
     *
     * @param config
     * @returns {*|void}
     */
    const handleAlertConfirm = function (config) {
        return swal.fire({
            type: handleIsset(config.type, 'warning'),
            title: handleIsset(config.title, 'Wymagane potwierdzenie'),
            html: handleIsset(config.text, 'Ta akcja wymagana dodatkowego potwierdzenia'),
            showCancelButton: true,
            cancelButtonText: 'nie',
            confirmButtonText: 'tak',
        }).then(function (result) {
            if (result.value) {
                if (config.yes && typeof (config.yes) === 'function') {
                    config.yes.call(this);
                }
            }
        });
    };

    /**
     *
     * @param mixedlet
     * @returns {boolean}
     */
    const handleEmpty = function (mixedlet) {
        let undef, key, i, len, emptyValues = [undef, null, false, 0, '', '0']

        for (i = 0, len = emptyValues.length; i < len; i++) {
            if (mixedlet === emptyValues[i]) return true
        }

        if (typeof mixedlet === 'object') {
            for (key in mixedlet) {
                if (mixedlet.hasOwnProperty(key)) return false;
            }

            return true
        }

        return false
    };

    /**
     *
     * @param needle
     * @param haystack
     * @param argStrict
     * @returns {boolean}
     */
    const handleInArray = function (needle, haystack, argStrict) {
        let key = ''
        let strict = !!argStrict;

        // we prevent the double check (strict && arr[key] === ndl) || (!strict && arr[key] === ndl)
        // in just one for, in order to improve the performance
        // deciding wich type of comparation will do before walk array
        if (strict) {
            for (key in haystack) {
                if (haystack[key] === needle) {
                    return true
                }
            }
        } else {
            for (key in haystack) {
                if (haystack[key] == needle) { // eslint-disable-line eqeqeq
                    return true
                }
            }
        }

        return false
    };

    /**
     *
     * @param path
     * @returns {string}
     */
    const handleGetUrl = function (path) {
        return window.location.protocol + '//' + window.location.hostname + '/' + path;
    };

    /**
     *
     * @param config
     * @returns {*|void}
     */
    const handleCswal = function (config) {
        return swal.fire({
            title: config.title,
            html: config.text,
            type: config.type,

            allowOutsideClick: false
        });
    };

    /**
     *
     * @returns {URLSearchParams}
     */
    const handleGetURLParams = function () {
        return (new URL(document.location)).searchParams;
    };

    /**
     *
     * @returns {string}
     */
    const handleGetUrlPathName = function () {
        return window.location.pathname.substring(1);
    };

    /**
     *
     * @param config
     */
    const handleAjaxProblem = function (config) {
        swal.fire({
            title: config.title,
            type: 'error',
            html: !handleEmpty(config.info) ? config.text + '<br><br>' + config.info : config.text,

            showCancelButton: true,
            cancelButtonText: !handleEmpty(config.cancelButtonText) ? config.cancelButtonText : 'Zamknij',

            confirmButtonText: 'Ponów',
            showConfirmButton: !handleEmpty(config.confirmButton) ? config.confirmButton : true,
            allowOutsideClick: false
        }).then(function (result) {
            if (result.value) {
                if (config.callbackValue && typeof (config.callbackValue) === 'function') {
                    config.callbackValue.call(this);
                }
            } else {
                if (config.reload) {
                    location.reload();
                } else if (config.callback && typeof (config.callback) === 'function') {
                    config.callback.call(this);
                }
            }
        });
    };

    /**
     *
     * @param email
     * @returns {boolean}
     */
    const handleValidEmail = function (email) {
        const regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex_email.test(email);
    };

    /**
     *
     * @param phone
     * @returns {boolean}
     */
    const handleValidPhone = function (phone) {
        const regex_phone = /^([0-9]{9})$/;
        return regex_phone.test(phone);
    };

    /**
     *
     * @param config
     */
    const handleAjaxStandard = function (config) {
        let $btn = config.btn;
        let $form = $('.auth-forms');

        let preloaderButton = handleIsset(config.preloaderButton, false);
        let preloader = handleIsset(config.preloader, true);

        $().Ajax().load({
            preloader: {
                show: false,
            },

            source: {
                method: config.method,
                routeName: config.routeName,
                cache: config.cache,
                processData: config.processData,
                contentType: config.contentType,
                data: config.data,

                before: function () {
                    if (preloader) {
                        preloaderButton ? $btn.preloaderButton().show() : $form.preloaderForm().show();
                    }
                },

                done: function (response) {
                    if ($btn !== undefined) {
                        $btn.removeClass('stoped');
                    }

                    switch (response.status) {
                        case true :
                            if (preloader) {
                                preloaderButton ? $btn.preloaderButton().hide() : $form.preloaderForm().hide();
                            }

                            if (config.success && typeof (config.success) === 'function') {
                                return config.success.call(this, response.data);
                            }
                            break;

                        case false:
                            if (preloader) {
                                preloaderButton ? $btn.preloaderButton().hide() : $form.preloaderForm().hide();
                            }

                            App.alert(response.error);
                            break;

                        default :
                            if (preloader) {
                                preloaderButton ? $btn.preloaderButton().hide() : $form.preloaderForm().hide();
                            }

                            App.alert('Wystąpił nieoczekiwany problem! Odswież stronę i spróbuj ponownie!');
                    }
                },

                fail: function () {
                    if ($btn !== undefined) {
                        $btn.removeClass('stoped');
                    }

                    if (preloader) {
                        preloaderButton ? $btn.preloaderButton().hide() : $form.preloaderForm().hide();
                    }
                }
            }
        });
    };

    const handleToPrevPage = function () {
        window.history.back();
    };

    /**
     *
     * @returns {string}
     */
    const handlePrevPage = function () {
        return document.referrer;
    };

    /**
     *
     * @returns {string}
     */
    const handleRand = function () {
        return Math.random().toString(36).slice(2);
    };

    /**
     *
     * @param str
     * @returns {number}
     */
    const handleHashCode = function (str) {
        let hash = 0, i, chr;

        for (i = 0; i < str.length; i++) {
            chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }

        return hash;
    };

    const handleAnchor = function () {
        let $root = $('html, body');

        $('.anchor').on('click', function (e) {
            e.preventDefault();

            $root.animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, 500);

            return false;
        });
    };

    return {
        /**
         *
         * @param text
         * @param title
         * @param type
         * @returns {*|void}
         */
        alert: function (text, title, type) {
            return handleAlert(text, title, type);
        },

        /**
         *
         * @param config
         * @returns {*|void}
         */
        alertConfirm: function (config) {
            return handleAlertConfirm(config);
        },

        /**
         *
         * @param mixedlet
         * @returns {boolean}
         */
        empty: function (mixedlet) {
            return handleEmpty(mixedlet);
        },

        /**
         *
         * @param needle
         * @param haystack
         * @param argStrict
         * @returns {boolean}
         */
        inArray: function (needle, haystack, argStrict) {
            return handleInArray(needle, haystack, argStrict);
        },

        /**
         *
         * @param issetlet
         * @param caselet
         * @returns {*}
         */
        isset: function (issetlet, caselet) {
            return handleIsset(issetlet, caselet);
        },

        /**
         *
         * @param path
         * @returns {string}
         */
        getUrl: function (path) {
            return handleGetUrl(path);
        },

        /**
         *
         * @param config
         * @returns {*|void}
         */
        cswal: function (config) {
            return handleCswal(config);
        },

        /**
         *
         * @returns {URLSearchParams}
         */
        getURLParams: function () {
            return handleGetURLParams();
        },

        /**
         *
         * @returns {string}
         */
        getUrlPathName: function () {
            return handleGetUrlPathName();
        },

        /**
         *
         * @param config
         * @constructor
         */
        AjaxProblem: function (config) {
            return handleAjaxProblem(config);
        },

        /**
         *
         * @param email
         * @returns {boolean}
         */
        ValidEmail: function (email) {
            return handleValidEmail(email);
        },

        /**
         *
         * @param phone
         * @returns {boolean}
         */
        ValidPhone: function (phone) {
            return handleValidPhone(phone);
        },

        /**
         *
         * @param config
         * @constructor
         */
        AjaxStandard: function (config) {
            return handleAjaxStandard(config);
        },

        toPrevPage: function () {
            return handleToPrevPage();
        },

        /**
         *
         * @returns {string}
         */
        prevPage: function () {
            return handlePrevPage();
        },

        /**
         *
         * @returns {string}
         */
        rand: function () {
            return handleRand();
        },

        /**
         *
         * @param str
         * @returns {number}
         */
        hashCode: function (str) {
            return handleHashCode(str);
        },

        anchor: function () {
            handleAnchor();
        }
    }
}();


//------------------------------------------------------------------
// PLUGINS
//------------------------------------------------------------------
(function ($) {
    $.fn.preloaderButton = function () {
        const $parent = $(this).parent();

        return {
            show: function (callback) {
                let view = '<div class="loader-circle-small preloader-button" style="margin-left:10px"></div>';

                $parent.append(view);
                $parent.css({
                    display: 'flex',
                    'align-items': 'center',
                });

                if (callback && typeof (callback) === 'function') {
                    callback.call(this);
                }
            },

            hide: function (callback) {
                $parent.find('.preloader-button').fadeOut(300, function () {
                    if (callback && typeof (callback) === 'function') {
                        callback.call(this);
                    }

                    $(this).remove();
                });
            }
        }
    };

    /**
     *
     * @returns {{hide: hide, show: show}}
     */
    $.fn.preloaderData = function () {
        const $t = $(this);
        const $preloader = $('#preloader');

        const sleep = function (milliseconds) {
            let start = new Date().getTime();

            for (let i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds) break;
            }
        };

        return {
            show: function (fadeIn, callback) {
                if (App.empty($preloader)) {
                    let $view = '<div class="preloader" id="preloader"><div class="loader-circle"></div><span>Trwa ładowanie danych...</span></div>';

                    if (fadeIn == true) {
                        $view = $($view);
                        $view.hide().fadeIn(300);
                    }

                    if ($t.is('body')) $('body').addClass('overflow-hidden');
                    $t.prepend($view);
                }

                if (callback && typeof (callback) === 'function') {
                    callback.call(this);
                }
            },

            hide: function (callback) {
                sleep(300);

                if (!App.empty($preloader)) {
                    $preloader.fadeOut(300, function () {
                        $(this).remove();

                        if ($t.is('body')) $('body').removeClass('overflow-hidden');
                        if (callback && typeof (callback) === 'function') {
                            callback.call(this);
                        }
                    });
                }
            }
        };
    };

    /**
     *
     * @returns {{hide: hide, show: show}}
     */
    $.fn.preloaderForm = function () {
        const $t = $(this);

        return {
            show: function (callback) {
                let view = '<div class="preloader" id="preloader"><div class="loader-circle"></div></div>';
                $t.append(view);

                if (callback && typeof (callback) === 'function') {
                    callback.call(this);
                }
            },

            hide: function (callback) {
                $t.find('.preloader').fadeOut(300, function () {
                    if (callback && typeof (callback) === 'function') {
                        callback.call(this);
                    }
                });
            }
        }
    };

    /**
     *
     * @returns {{reload: reload, load: load, params: params}}
     * @constructor
     */
    $.fn.Ajax = function () {
        let $t = $(this);

        // Temp Ajax Data
        let loadSettings = {};

        const load = function (config) {
            let settings = $.extend({
                preloader: true,
                failName: null,
                statutes: {},
                source: {},
                text: {}
            }, config);

            let success = false;
            let preloader = App.isset(settings.preloader.show, true);

            let routeName = App.isset(settings.source.routeName, '');
            let cache = App.isset(settings.source.cache, true);
            let processData = App.isset(settings.source.processData, true);
            let dataType = App.isset(settings.source.dataType, 'json');
            let contentType = App.isset(settings.source.contentType, 'application/x-www-form-urlencoded;charset=UTF-8');
            let method = App.isset(settings.source.method, 'POST');
            let allowed = App.isset(settings.statutes.allowed, [true, null]);
            let getStatus = App.isset(settings.statutes.getStatus, false);

            let none_data = App.isset(settings.text.none_data, 'Brak danych do wyświetlenia.');

            loadSettings = {
                preloader: {
                    show: preloader,
                    before: settings.preloader.before
                },

                failName: settings.failName,
                statutes: {
                    allowed: allowed,
                    getStatus: getStatus
                },

                source: {
                    routeName: routeName,
                    dataType: dataType,
                    contentType: contentType,
                    processData: processData,
                    cache: cache,
                    method: method,
                    data: settings.source.data,
                    before: settings.source.before,
                    done: settings.source.done,
                    fail: settings.source.fail,
                    reload: settings.source.reload
                },

                text: {
                    none_data: none_data
                }
            };

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: laroute.route(routeName),
                method: method,
                dataType: dataType,
                contentType: contentType,
                processData: processData,
                cache: cache,
                data: settings.source.data,

                beforeSend: function () {
                    // Reload Ajax
                    if (settings.source.reload && typeof (settings.source.reload) === 'function') {
                        settings.source.reload.call(this);
                    }

                    // Preloader
                    if (settings.source.before && typeof (settings.source.before) === 'function') {
                        return settings.source.before.call(this);
                    } else if (preloader == true) {
                        $t.preloaderData().show(true);
                    }
                },
            }).done(function (response) {
                $('.form-valid-alert').remove();

                if (response.status == -10) {
                    App.cswal({
                        title: 'Twoja sesja wygasła!',
                        text: 'Musisz ponownie się zalogować, aby kontynuuować operację.',
                        type: 'error'
                    }).then(function () {
                        location.reload()
                    });
                } else if (allowed.indexOf(response.status) != -1) {
                    if (preloader == true) {
                        if (settings.preloader.before && typeof (settings.preloader.before) === 'function') {
                            return settings.preloader.before.call(this);
                        }

                        $t.preloaderData().hide(function () {
                            if ((response.status == null || (App.empty(response.data) && response.data !== undefined)) && none_data != false) {
                                $t.append('<div style="text-align:center"><p style="margin:10px 0">' + none_data + '</p></div>');
                            }

                            if (settings.source.done && typeof (settings.source.done) === 'function') {
                                return settings.source.done.call(this, !getStatus ? response.data : response);
                            }
                        });
                    } else {
                        if (settings.source.done && typeof (settings.source.done) === 'function') {
                            return settings.source.done.call(this, !getStatus ? response.data : response);
                        }
                    }

                    success = true;
                } else {
                    App.AjaxProblem({
                        title: 'Wystąpił nieoczekiwany problem',
                        text: 'Wystąpił nieoczekiwany problem, spróbuj ponownie wysłać żądanie badź skontaktuj się z adnimistratorem.',

                        callbackValue: function () {
                            reload();
                        },

                        callback: function () {
                            $t.preloaderData().hide();
                        }
                    });
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                App.AjaxProblem({
                    title: 'Bład połączenia z serwerem!',
                    text: window.navigator.onLine ?
                        'Wystąpił problem z serwerem i Twoje żądanie nie może zostać obsłużone! Spróbuj ponownie wysłać żądanie, bądź skontakuj się z administratorem.' :
                        'Sprawdź swoje połączenie z internetem.',

                    callbackValue: function () {
                        reload();
                    },

                    callback: function () {
                        $t.preloaderData().hide();
                    }
                });

                if (settings.source.fail && typeof (settings.source.fail) === 'function') {
                    return settings.source.fail.call(this, jqXHR, textStatus, errorThrown);
                }
            });

            return success;
        };


        /**
         *
         * Overloading the load method.
         *
         * @return void
         * @package GetAjaxDataInfo
         *
         */
        const reload = function () {
            load(loadSettings);
        };


        /**
         *
         * Sets of new parameters
         * to the load method.
         *
         * @return void
         *
         * @package GetAjaxDataInfo
         *
         * @param config
         */
        const params = function (config) {
            for (let key in config) loadSettings.data[key] = config[key];
            reload();
        };


        return {
            load: function (settings) {
                load(settings)
            },
            params: function (settings) {
                params(settings)
            },
            reload: function () {
                reload()
            }
        };
    }
})(jQuery);
