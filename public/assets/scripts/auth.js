// Auth scripts
const Auth = function () {
    const handleForm = function ($form, config) {
        let isOk = true;
        let text = 'Uzupełnij wymagane pola!';

        let $btn = $form.find('button');
        const $required = $form.find('.required');

        $required.each(function () {
            let $t = $(this);

            if ($t.val() === '') {
                $t.addClass('input-danger');
                isOk = false;
            }
        });

        if (isOk) {
            if (App.ValidEmail($('#email').val()) === false) {
                isOk = false;
                text = 'Podany adres e-mail jest nieprawidłowy!';
            }

            if (config.register === true) {
                if ($('#password').val() !== $('#password-confirm').val()) {
                    isOk = false;
                    text = 'Podane hasła są różne!';
                }
            }

            if (isOk) {
                if ($btn.hasClass('stoped')) {
                    return false;
                }

                $btn.addClass('stoped');

                App.AjaxStandard({
                    btn: $btn,
                    routeName: config.routeName,
                    data: config.data,

                    success: function () {
                        location.href = config.redirect;
                    }
                });
            }
        }

        if (!isOk) {
            App.alert(text);
        }

        $required.on('keyup change', function () {
            $(this).removeClass('input-danger');
        });
    };

    const handleLogin = function () {
        const $form = $('#form-login');

        if ($form.length > 0) {
            $form.on('submit', function (e) {
                e.preventDefault();

                handleForm($form, {
                    routeName: 'auth.login.ajax',
                    data: {
                        email: $('#email').val(),
                        password: $('#password').val(),
                    },

                    redirect: '/',
                });
            });
        }
    };

    const handleRegister = function () {
        const $form = $('#form-register');

        if ($form.length > 0) {
            $form.on('submit', function (e) {
                e.preventDefault();

                handleForm($form, {
                    register: true,
                    routeName: 'auth.register.ajax',
                    data: {
                        name: $('#name').val(),
                        email: $('#email').val(),
                        password: $('#password').val(),
                    },

                    redirect: laroute.route('auth.login.index'),
                });
            });
        }
    };

    return {
        init: function () {
            handleLogin();
            handleRegister();
        }
    }
}();

$(function () {
    Auth.init();
});
