@extends('layouts.auth')
@section('title', 'Logowanie')

@section('form-title', 'Zaloguj się')

@section('main-form')
    <form method="post" id="form-login" style="color: #757575;" action="/">
        @if (session('warning'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ session('warning') }}

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <!-- E-mail -->
        <div class="md-form mt-3">
            <input type="email" class="form-control required" id="email">
            <label for="email">Adres e-mail</label>
        </div>

        <!-- Password -->
        <div class="md-form">
            <input type="password" id="password" class="form-control required">
            <label for="password">Hasło</label>
        </div>

        <!-- Sign in button -->
        <button
            class="btn btn-outline-primary btn-rounded btn-block z-depth-0 my-4 waves-effect"
            type="submit"
        >Zaloguj się
        </button>

        <p>Nie masz konta?
            <a href="{{ route('auth.register.index') }}">Zarejestruj się</a>
        </p>
    </form>
@endsection
