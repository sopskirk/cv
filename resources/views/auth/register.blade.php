@extends('layouts.auth')
@section('title', 'Rejestracja')

@section('form-title', 'Zarejestruj się')

@section('main-form')
    <form method="post" id="form-register" style="color: #757575;" action="/">
        <!-- Name -->
        <div class="md-form mt-3">
            <input class="form-control required" id="name">
            <label for="name">Nazwa użytkownika</label>
        </div>

        <!-- Email -->
        <div class="md-form mt-3">
            <input type="email" class="form-control required" id="email">
            <label for="email">Adres e-mail</label>
        </div>

        <!-- Password -->
        <div class="md-form">
            <input type="password" id="password" class="form-control required">
            <label for="password">Hasło</label>
        </div>

        <!-- Password Confirm -->
        <div class="md-form">
            <input type="password" id="password-confirm" class="form-control required">
            <label for="password-confirm">Potwierdź hasło</label>
        </div>

        <!-- Sign in button -->
        <button
            class="btn btn-outline-primary btn-rounded btn-block z-depth-0 my-4 waves-effect"
            type="submit"
        >Zarejestruj się</button>

        <p>Masz już konto?
            <a href="{{ route('auth.login.index') }}">Zaloguj się</a>
        </p>
    </form>
@endsection
