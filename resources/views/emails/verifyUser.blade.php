@extends('layouts.email')
@section('title', 'Weryfikacja adresu e-mail')

@section('main-content')
    <body style="width:100%;height:100%;margin:0;padding:0;font-family:Helvetica,serif">
        <div style="padding:40px;background:#fafafa;border:1px solid #e5e5e5;border-radius:10px;width:fit-content;margin:auto">

            <p style="margin:0 0 10px;padding:0"><b>Nazwa użytkownika</b>: {{ $user->name }}</p>
            <p style="margin:0 0 10px;padding:0"><b>Adres e-mail</b>: {{ $user->email }}</p>

            <p style="margin:25px 0 0;padding:0;text-align:center">
                <a href="{{ route('auth.register.verify', $user->verify->token) }}" style="
                    color: white;
                    background-color: #3e416d;
                    display: inline-block;
                    font-weight: 600;
                    text-align: center;
                    vertical-align: middle;
                    border: 1px solid transparent;
                    padding: 10px 17px;
                    line-height: 17px;
                    font-size: 13px;
                    border-radius: 6px;
                    text-decoration: none;
                ">Potwierdź adres e-mail</a>
            </p>

            <p style="margin:25px 0 0;padding:0;text-align:right">
                <span style="color:#999;font-size:12px">{{ env('APP_NAME').' '.date('Y') }}</span>
            </p>
            <p style="margin: 15px 0 0;padding:0;max-width:400px">
                <span style="color:#999;font-style:italic;font-size:11px">Ta wiadomość została wygenerowana automatycznie i prosimy na nią nie odpowiadać.</span>
            </p>

        </div>
    </body>
@endsection
