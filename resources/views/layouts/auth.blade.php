<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title>{{ config('app.name') }} - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Keos Media">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts
    ================================================== -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.css') }}">
    <link rel="stylesheet" href="{{ version('styles/app.css') }}">
    <link rel="stylesheet" href="{{ version('styles/auth.css') }}">

</head>

<body>

<!-- Content
================================================== -->
<div class="auth-container">
    <div class="auth-forms">
        <div class="card">
            <h5 class="card-header info-color white-text text-center py-4">
                <strong>@yield('form-title')</strong>
            </h5>

            <!-- begin:: Form -->
            <div class="card-body px-lg-5">
                @section('main-form')
                @show
            </div>
            <!-- end:: Form -->
        </div>
    </div>
</div>

<!-- JS
================================================== -->
<script src="{{ asset('plugins/jquery-3.5.0.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('laravel/laroute.js') }}"></script>
<script src="{{ version('plugins/sweetalert2/init.js') }}"></script>
<script src="{{ version('scripts/app.js') }}"></script>
<script src="{{ version('scripts/auth.js') }}"></script>
</body>

