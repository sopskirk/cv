@extends('layouts.app')
@section('title', 'Strona główna')

@section('main-content')
    <p>Witaj <strong>{{ $user->name }}</strong></p>
    <p><a href="{{ route('auth.logout') }}">Wyloguj się</a></p>
@endsection
