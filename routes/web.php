<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home Page
Route::get('/', static function () {
    return redirect()->route('auth.login.index');
});

// Auth Pages
Route::group(
    ['prefix' => '/auth', 'as' => 'auth.', 'namespace' => 'Auth'],
    static function () {
        Route::group(
            ['middleware' => 'auth.redirect'],
            static function () {
                // Login
                Route::group(
                    ['prefix' => '/login', 'as' => 'login.'],
                    static function () {
                        // Login Page
                        Route::get('/', 'LoginController@index')
                            ->name('index');

                        // Logging
                        Route::group(
                            ['laroute' => true],
                            static function () {
                                Route::post('/', 'LoginController@ajax')
                                    ->name('ajax');
                            }
                        );
                    }
                );

                // Register
                Route::group(
                    ['prefix' => '/register', 'as' => 'register.'],
                    static function () {
                        // Register Page
                        Route::get('/', 'RegisterController@index')
                            ->name('index');

                        // Verify Page
                        Route::get('/verify/{token}', 'RegisterController@verify')
                            ->name('verify');

                        // Registration
                        Route::group(
                            ['laroute' => true],
                            static function () {
                                Route::post('/', 'RegisterController@ajax')
                                    ->name('ajax');
                            }
                        );
                    }
                );
            }
        );

        // Logout
        Route::group(
            ['middleware' => 'auth.app'],
            static function () {
                Route::get('/logout', 'LogoutController@logout')
                    ->name('logout');
            }
        );
    }
);

// After Logging
Route::group(
    ['as' => 'logged.', 'namespace' => 'Logged', 'middleware' => 'auth.app'],
    static function () {
        // Home Page
        Route::get('/', 'HomeController@index')->name('index');
    }
);
